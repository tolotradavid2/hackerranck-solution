function pickingNumbers(a: number[]) {
    // Write your code here
    let res: number[][] = [];
    let k = 0
    for (let i = 0; i < a.length; i++) {
        res[k] = [a[i]]
        console.log(res);
        for (let j = 0; j < a.length; j++) {    
            if(i != j && res[k].every(x => Math.abs(x - a[j] ) <=1 ) ) {
                res[k].push(a[j])
            }
        }
        ++k
    }
    
    return res.reduce((l, p) =>  p.length > l ? p.length : l,0);
}
console.log(pickingNumbers([4, 6, 5, 3, 3, 1]));