// OK

const jumpingOnClouds = (c: Array<number>) => {
    let index = 0;
    let jumps = 0;

    do{
        c[index + 2] === 0 ? index += 2 : index++
        jumps++
    }while(index < c.length - 1)

    return jumps
}



console.log(jumpingOnClouds([0, 1, 0, 0, 0, 1, 0])); //3
console.log(jumpingOnClouds([0, 0, 1, 0, 0, 1, 0])); //4
console.log(jumpingOnClouds([0, 0, 0, 0, 1, 0])); //3