function beautifulDays(i: number, j: number, k: number): number {
    // Write your code here
    let counter = 0;
    for (let l = i; l <= j; l++) {
        let diff = Math.abs((l - (parseInt(l.toString().split('').reverse().join('')))))
        if(Number.isInteger(diff / k)) counter++
    }
    return counter;
}

console.log(beautifulDays(20, 23, 6));