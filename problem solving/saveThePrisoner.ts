function saveThePrisoner(n: number, m: number, s: number): number {
    // Write your code here
    return ((s + m) - 1) % n || n
}

console.log(saveThePrisoner(5, 2, 1));
console.log(saveThePrisoner(5, 2, 2));
console.log(saveThePrisoner(7, 19, 2));
console.log(saveThePrisoner(3, 7, 3));