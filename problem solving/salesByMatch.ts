// OK sady mazava
const sockMerchant = (n: number, ar: Array<number>) => {
    let socks: any = {}
    let pairs: number  = 0

    if(n !== ar.length) return

    for (const element of ar) {
        socks[element] = socks[element] + 1 || 1
        if(socks[element] % 2 === 0) pairs++
    }

    return pairs
}

console.log(sockMerchant(7, [1, 2, 1, 2, 1, 3, 2])); // 2
// console.log(sockMerchant(9, [10, 20, 20, 10, 10, 30, 50, 10, 20])); // 3