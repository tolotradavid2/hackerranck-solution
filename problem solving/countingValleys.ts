// OK
const countingValleys = (steps: number, path: string) => {
    const pathNumber = path.split('').map(n => {
        return (n === 'U') ? 1 : -1
    })
    let sum = 0
    let valleys = 0

    if (steps !== path.length) return

    for (let i = 0; i < pathNumber.length; i++) {
        sum += pathNumber[i]
        if (pathNumber[i] >= sum && sum === 0) {
            valleys++
        }
    }

    return valleys
}

console.log(countingValleys(8, 'UDDDUDUU')); // 1
console.log(countingValleys(8, 'DDUUUUDD')); // 1
console.log(countingValleys(12, 'DDUUDDUDUUUD'));

