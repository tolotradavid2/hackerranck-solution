function migratoryBirds(arr: number[]): number {
    // Write your code here
    let counts: any = {};
    let max = 0;
    let indexes: number[] = [];
    for (const num of arr) {
        counts[num] = counts[num] ? counts[num] + 1 : 1;
        if (counts[num] > max) max = counts[num];
    }
    let entries = Object.entries(counts);
    for (const test of entries) {
        if(test [1] == max) indexes.push(parseInt(test[0]))
    }
    return indexes.sort()[0];
}

console.log(migratoryBirds([1, 2, 3, 4, 5, 4, 3, 2, 1, 3, 4]));
console.log(migratoryBirds([1, 4, 4, 4, 5, 3]));