function dayOfProgrammer(year: number): string {
    const daysOfMonth = [31, 28, 31, 30, 31, 30, 31, 31];
    if (year <= 1917) {
        if (year % 4 == 0) {
            daysOfMonth[1] = 29;
        }
    } else if (year > 1918) {
        if (year % 4 == 0 && year % 100 != 0 || year % 400 == 0) {
            daysOfMonth[1] = 29;
        }
    } else if (year == 1918) {
        daysOfMonth[1] = 29;
        daysOfMonth[1] = daysOfMonth[1] - 14;
    }

    return `${256 - daysOfMonth.reduce((prev, curr) => prev + curr)}.09.${year}`;
}

console.log(dayOfProgrammer(1918));