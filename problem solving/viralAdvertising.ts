function viralAdvertising(n: number): number {
    let shared = 5
    let liked = 2 
    let cumulative = 0

    for(let i = 1; i <= n; i++){
        liked = Math.floor(shared / 2)
        shared = liked * 3
        cumulative = cumulative + liked
    }
    return cumulative;
}

console.log(viralAdvertising(5));