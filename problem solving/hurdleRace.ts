function hurdleRace(k: number, height: number[]): number {
    // Write your code here
    const max = height.reduce((prev, next) => prev > next ? prev : next, height[0]);
    const diff = max - k;
    if (diff > 0) return diff;
    return 0;
}

console.log(hurdleRace(4, [1, 6, 3, 5, 2]));
console.log(hurdleRace(7, [2, 5, 4, 5, 2]));