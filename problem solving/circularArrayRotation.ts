function circularArrayRotation(a: number[], k: number, queries: number[]): number[] {
    // Write your code here
    let i = 0;
    let ends: number[] = [];
    while (i < k) {
        a.unshift(a.pop() ?? 0);
        i++;
    }
    for (let i = 0; i < queries.length; i++) {
        ends.push(a[queries[i]]);
    }
    return ends;
}

console.log(circularArrayRotation([1, 2, 3], 2, [0, 1, 2]));