function getMoneySpent(b: number, n: number[], m: number[]): number {
    let max = 0
    for (let i = 0; i < n.length; i++) {
        for (let j = 0; j < m.length; j++) {
            if(b >= n[i] + m[j] && n[i] + m[j] > max ){
                max = n[i] + m[j]
            }
        }
    }
    if(max != 0 ){
        return max
    }
    return -1;
}

console.log(getMoneySpent(10, [3, 1], [5, 2, 8]));