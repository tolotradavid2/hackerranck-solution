function catAndMouse(x, y, z) {

    let distanceA = Math.abs(z - x);
    let distanceB = Math.abs(z - y);

    if (distanceA < distanceB) {
        console.log('Cat A');
    } else if (distanceA > distanceB) {
        console.log('Cat B');
    }else{
        console.log('Mouse C');
    }

}

catAndMouse(1, 2, 3);