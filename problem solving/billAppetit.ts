function bonAppetit(bill: number[], k: number, b: number): void {
    // Write your code here
    let totalAmount = bill.reduce((prev, next) => prev + next) - bill[k]
    let bActual = totalAmount / 2
    if(bActual == b){
        console.log('Bon Appetit');
    }else{
        console.log(Math.abs(bActual - b));
    }
}

bonAppetit([3, 10, 2, 9], 1, 12);