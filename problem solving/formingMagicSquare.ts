function formingMagicSquare(s: number[][]): number {
    // Write your code here
    const magic = 15;
    let cost: number[] = [];
    for (let i = 0; i < s.length; i++) {
        let sum = 0;
        for (let j = 0; j < s[i].length; j++) {
            sum += s[i][j];
        }
        if (sum != magic) {
            cost.push(Math.abs(magic - sum));
        }
    }
    return cost.reduce((prev, next) => prev + next);
}

console.log(formingMagicSquare([[5, 3, 4], [1, 5, 8], [6, 4, 2]]));