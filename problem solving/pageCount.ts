function pageCount(n: number, p: number): number {
    // Write your code here
    let left = Math.abs((0 - p) / 2);
    let right = Math.abs((n - p) / 2);
    if(n % 2 == 0 && p - n == -1){
        right = 1
    }
    if (right < left) {
        return Math.trunc(right);
    }
    return Math.trunc(left);
}

console.log(pageCount(6, 5));