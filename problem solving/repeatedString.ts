// OK
const repeatedString = (s: string, n: number) => {
    let countA = 0
    let restA = 0
    const stest = s.slice(0, n % s.length)
    for (let i = 0; i < s.length; i++) {
        if (s[i] === 'a' && s[i]) countA++
        if (stest[i] === 'a' && stest[i]) restA++
    }
    return Math.round(n / s.length) * countA + restA
}
console.log(repeatedString('aba', 10)); //7
console.log(repeatedString('bba', 10)); //3
console.log(repeatedString('a', 10000000)); // 10000000
console.log(repeatedString('abcac', 10)); // 4
console.log(repeatedString('ceebbcb', 817723)); //0
console.log(repeatedString('gfcaaaecbg', 547602)); //164280
console.log(repeatedString('kmretasscityylpdhuwjirnqimlkcgxubxmsxpypgzxtenweirknjtasxtvxemtwxuarabssvqdnktqadhyktagjxoanknhgilnm', 736778906400));
