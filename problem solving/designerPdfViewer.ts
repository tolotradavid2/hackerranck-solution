function designerPdfViewer(h: number[], word: string): number {
    // Write your code here
    const alphabet: string[] = [...'abcdefghijklmnopqrstuvwxyz'];
    const arrWords = word.split("");
    const indexes: number[] = [];
    let max = h[0];
    const values: number[] = [];
    arrWords.map(value => {
        indexes.push(alphabet.findIndex(element => element == value));
    });
    indexes.map(value => {
        values.push(h[value]);
    });
    max = values.reduce((p, n) => n > p ? n : p, 0);
    return max * word.length;
}

console.log(designerPdfViewer([1, 3, 1, 3, 1, 4, 1, 3, 2, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5], 'abc'));
console.log(designerPdfViewer([6, 5, 7, 3, 6, 7, 3, 4, 4, 2, 3, 7, 1, 3, 7, 4, 6, 1, 2, 4, 3, 3, 1, 1, 3, 5], 'zbkkfhwplj'));