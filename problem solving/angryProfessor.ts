function angryProfessor(k: number, a: number[]): string {
    // Write your code here
    if (a.filter(v => v <= 0).length < k) {
        return 'YES';
    } else {
        return 'NO';
    }
}

console.log(angryProfessor(3, [-1, -3, 4, 2]));
console.log(angryProfessor(2, [0, -1, 2, 1]));