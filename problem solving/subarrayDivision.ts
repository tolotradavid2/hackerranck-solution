function birthday(s: number[], d: number, m: number) {
    let counter = 0;
    for (let i = 0; i < s.length; i++) {
        let tmp = 0;
        for (let j = i; j < m + i; j++) {
            if(s[j]){
                tmp += s[j]
            }
        }
        if(tmp == d){
            counter++
        }
    }
    return counter;
}

console.log(birthday([1, 2, 1, 3, 2], 3, 2));